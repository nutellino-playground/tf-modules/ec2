# ec2 module

Original work by : https://github.com/cloudposse/terraform-aws-ec2-instance

## Usage


```hcl-terraform
module "instance" {
  source                      = "git::https://gitlab.com/nutellino-playground/tf-modules/ec2.git?ref=0.0.1"
  ssh_key_pair                = "${var.ssh_key_pair}"
  instance_type               = "t2.micro"
  vpc_id                      = "${module.vpc.vpc_id}"
  security_groups             = ["${aws_security_group.instance.id}"]
  subnet                      = "${element(split(",", module.vpc.subnet_ids),0)}"
  name                        = "instance"
  ami                         = "ami-0c0a804957898c597"
  ami_owner                   = "801119661308"
  namespace                   = "instance"
  stage                       = "prod"
}
```
